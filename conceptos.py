#Creacion de una lista compuesta 2x2
"""
    a b
    b d
"""
#numpy
lista_compuesta: list = [ ["a","b"], ["c", "d"] ]
#Fila cero
print( lista_compuesta[0] )
#Obtenemos el valor de la columna cero
print( lista_compuesta[0][0]+" "+lista_compuesta[1][1] )
print( lista_compuesta[1][1] )
#
print( "".join(lista_compuesta[0]) )


"""
---------------COMPARATIVA ENTRE NUMPY Y LISTAS COMPUESTAS---------------------
"""
print("---------------COMPARATIVA ENTRE NUMPY Y LISTAS COMPUESTAS---------------------")
#Importamos numpy
import numpy as np
#Importamos libreria del sistema
import sys
#Creamos un rango de numeros
rango = range(10000)
#Imprimir el espacio en memoria que ocupa una lista de mil elementos
print( sys.getsizeof(1)*len(rango) )

#Creamos una matriz de una fila con mil columnas
matriz = np.arange(10000)
print( matriz.size*matriz.itemsize )
print(2)
print("-----------------CALCULANDO EL TIEMPO DE EJECICION--------")
import time #Librería que nos permite trabajar con el tiempo del sistema
tamanio: int = 10000000
l1 = range(tamanio)
l2 = range(tamanio)
result: list = []
index = 0
tiempo_inicial = time.time()#Obtener el tiempo en milisegundos
for n in l1:
    suma = n + l2[ index ]
    result.append(suma)
    index += 1

print(time.time() - tiempo_inicial)

m1 = np.arange(tamanio)
m2 = np.arange(tamanio)
tiempo_inicial = time.time()
suma = m1+m2
print(time.time()-tiempo_inicial)

"""
Para los que tienen python 3:
pip3 install numpy
"""

