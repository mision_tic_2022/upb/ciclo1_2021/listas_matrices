"""
Instalar pip: py -mi_version_python -m pip install numpy
pip install numpy
"""
import numpy as np

#Crear una matriz o arreglo
matriz = np.array([1, 2, 3, 4])
#Lista
numeros: list = [1,2,3,4]

print(numeros)
print(matriz)
print("-----------------------")
#Matriz de varias filas y columnas
lista_compuesta: list = [ [1,2], [3,4] ]
matriz = np.array( [ [1,2], [3,4] ] )
print(lista_compuesta)
print(matriz)
print("--------------------------------")
#--------------------------------
lista_1: list = [1,2,3,4]
lista_2: list = [5,6,7,8]
resultado_listas = lista_1 + lista_2
print("resultado_listas: ", resultado_listas)

matriz_1 = np.array([1,2,3,4])
matriz_2 = np.array([5,6,7,8])
resultado_matriz = matriz_1 + matriz_2
print("resultado_matriz: ", resultado_matriz)

m1 = np.array([ [5,2], [10, 8] ])
m2 = np.array([ [1,4], [6, 8] ])
suma_matriz = m1 + m2
print(suma_matriz)